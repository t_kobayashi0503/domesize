﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DomeSize
{
    //◇デザイン要素一覧

    //【比較対象物の選択】
    //[コントロール]ComboBox:東京ドーム、エレン・イェーガー、富士山
    //[プロパティ]Name:rulerList [イベント]SelectedIndexChanged:RulerListChanged

    //【押された数字を表示する箇所】
    //[コントロール]TextBox:押された数字が表示される
    //[プロパティ]Name:calcNumText [イベント]なし

    //【数字ボタン】
    //[コントロール]Button:各数字ボタン
    //[プロパティ]Text:各数字 Name:デフォルトのまま [イベント]Click:NumButtonClick

    //【クリアボタン】
    //[コントロール]Button:Cボタン
    //[プロパティ]Text:C Name:clearBtn [イベント]Click:ClearBtnClick

    //【計算ボタン】
    //[コントロール]Button:計算ボタン
    //[プロパティ]Text:計算 Name:calcBtn [イベント]Click:CalcBtnClick

    //【名前ラベル】
    //[コントロール]Label:選択された比較対象物の名前表示
    //[プロパティ]Name:rulerNameLabel [イベント]なし

    //【長さラベル】
    //[コントロール]Label:長さ表示
    //[プロパティ]Name:rulerSizeNumLabel [イベント]なし

    //【単位ラベル】
    //[コントロール]Label:単位表示
    //[プロパティ]Name:rulerUnitLabel [イベント]なし

    //【計算結果ラベル】
    //[コントロール]Label:結果の個数表示
    //[プロパティ]Name:resultNumLabel [イベント]なし

    //【ピクチャボックスを並べるパネル】
    //[コントロール]PictuerBox:選択された比較対象物の画像表示
    //[プロパティ]Name:imagePanel Size:480,240 [イベント]なし

    public partial class Form1 : Form
    {
        //基準値ルーラーリスト
        List<Ruler> rulers = new List<Ruler>();

        string imgSrc; //ルーラー画像ソース　Q.Ruler.csのものとは別物？なぜここに定義する必要があるのか
        string inputNumString; //入力された数字文字列　Q.なぜ大文字？
        int rulerNum; //ルーラー基準値

        public Form1()
        {
            InitializeComponent();
        }

        //（基準値）Rulerインスタンスの読み込み
        public void LoadRulerList()
        {
            //ルーラーインスタンスをリストに追加
            rulers.Add(new Ruler(0, "東京ドーム", 46755, "㎡", "1.png"));
            rulers.Add(new Ruler(1, "エレン・イェーガー", 17, "ｍ", "2.png"));
            rulers.Add(new Ruler(2, "富士山", 3376, "ｍ", "3.png"));

            //コンボボックスに表示
            foreach (Ruler r in rulers)
            {
                this.rulerList.Items.Add(r.Name);
            }
        }
        //ルーラー情報を表示
        private void ShowRulerInfo(int index)
        {
            //基準値を指定
            rulerNum = rulers[index].SizeNum;
            //各ラベル、変数に値表示
            rulerNameLabel.Text = rulers[index].Name; //ルーラー名 左辺がプロパティ、右辺がRuler.csの変数
            rulerSizeNumLabel.Text = rulerNum.ToString(); //ルーラー値
            rulerUnitLabel.Text = rulers[index].Unit; //ルーラー単位
            imgSrc = rulers[index].ImgSrc; //ルーラー画像
        }

        //複数のルーラー画像を表示
        private void ShowPictureBox(int resultImageNum)
        {
            //複数のルーラー画像をまとめるリスト
            List<PictureBox> pictures = new List<PictureBox>();

            for (int i = 0; i < resultImageNum; i++)
            {
                //画像コントロールのインスタンス作成
                pictures.Add(new PictureBox());

                //プロパティ設定
                pictures[i].Name = "pictureBox" + i.ToString(); //？
                pictures[i].SizeMode = PictureBoxSizeMode.StretchImage;
                //画像ファイルを読み込んで、Imageオブジェクトとして取得する
                pictures[i].Image = Image.FromFile(@"../../images/" + imgSrc); //画像URL
                pictures[i].Width = 64; //画像幅
                pictures[i].Height = 64; //画像高さ
                pictures[i].Left = i * 64; //？

                //コントロールをフォームに追加
                this.imagePanel.Controls.Add(pictures[i]); //フォームに画像を表示する
            }
        }

        //情報リセット
        private void ResetRulerInfo()
        {
            //計算数値をリセット
            inputNumString = ""; //入力されていた数値の文字列の中身をなくす。
            calcNumText.Text = inputNumString; //calcNumTextに何もない文字列を代入。
            resultNumLabel.Text = "?"; //resultNumLabelに？の文字列を代入。

            //パネル内画像を後ろから全削除
            for (int i = this.imagePanel.Controls.Count - 1; 0 <= i; i--)
            {
                this.imagePanel.Controls[i].Dispose();
            }
        }
        //開始ボタンを押すと最初にFormで実行されるメソッド
        private void Form1_Load(object sender, EventArgs e)
        {
            //情報リセット
            ResetRulerInfo();

            //基準値リストを読み込み
            LoadRulerList(); //（基準値）Rulerインスタンスの読み込み
            int index = 0; //ルーラーリスト初期値
            this.rulerList.SelectedIndex = index; //Q.206行目と左右逆？
            ShowRulerInfo(index); //ルーラー情報を表示
        }
        //イベントハンドラ：1～000のボタンクリック時
        private void NumButtonClick(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            //押されたボタンの数字
            string btnText = btn.Text;

            //[入力された数字]に連結する
            inputNumString += btnText;
            //画面上に数字を出す
            calcNumText.Text = inputNumString;
        }
        //イベントハンドラ：クリアボタンクリック時
        private void ClearBtnClick(object sender, EventArgs e)
        {
            ResetRulerInfo();
        }

        //イベントハンドラ：計算ボタンクリック時
        private void CalcBtnClick(object sender, EventArgs e)
        {
            //文字列を数値変換
            int value; //TryParse用変数
            int inputNum;//数値変換した値
            double resultNum;//計算結果

            //正しい数字が入力されているか判断し、
            //trueの場合、isNumberにtureとvalueを戻り値にする。
            bool isNumber = int.TryParse(calcNumText.Text, out value);

            if (isNumber) //isNumberがtureだった場合、inputNumにvalueを入れる。
            {
                inputNum = value;
            }
            else //isNumberがfalseだった場合、inputNumに0を入れる。
            {
                inputNum = 0;
            }

            //ルーラーを基準に計算してテキストボックスに表示
            resultNum = inputNum / rulerNum; //入力された数値÷比較対象の数値の結果をresultNumに代入。
            resultNumLabel.Text = resultNum.ToString(); //resultNumを文字列にしてresultNumLabelに代入。

            //結果画像表示数を制限
            int resultImageNum = (int)resultNum; //resultNumをint型にしてresultimageNumに代入。
            if (resultImageNum >= 18) //resultImageNumが18以上になった場合、resultImageNumは18とする
            {
                resultImageNum = 18;
            }
            //
            ShowPictureBox(resultImageNum); //引数resultImageNumを持つShowPictureBoxメソッドを実行。
        }

        //イベントハンドラ：コンボボックスの内容を変更した時
        private void RulerListChanged(object sender, EventArgs e)
        {
            //情報リセット
            ResetRulerInfo();
            //選択したルーラーリストの情報を表示
            int index = this.rulerList.SelectedIndex; //Q.143行目と左右逆？
            ShowRulerInfo(index); //ルーラー情報を表示
        }
    }
}
